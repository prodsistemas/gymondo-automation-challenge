const expect = require('chai').expect;
const searchActions = require('../actions/search.actions');
let actualResultItemsSize;

class SearchQuestions {
    async searchResultItemsShouldBe(expectedResultItemsSize) {
        actualResultItemsSize = await searchActions.getResultItemsSize();
        expect(actualResultItemsSize).equal(expectedResultItemsSize);
    }

    async searchResultItemsShouldBeMoreThan(expectedResultItemsSize) {
        actualResultItemsSize = await searchActions.getResultItemsSize();
        expect(actualResultItemsSize).to.be.above(expectedResultItemsSize);
    }
}
module.exports = new SearchQuestions();