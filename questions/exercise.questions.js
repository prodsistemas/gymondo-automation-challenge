const expect = require('chai').expect;
const exerciseActions = require('../actions/exercise.actions');

class ExerciseQuestions {
    async shouldDisplaySelectedExerciseContent(expectedExercise) {
        const actualExercise = await exerciseActions.getExerciseTitle();
        const isExerciseImageDisplayed = await exerciseActions.isExerciseImageDisplayed();
        expect(actualExercise).equal(expectedExercise);
        expect(isExerciseImageDisplayed).equal(true);
    }
}
module.exports = new ExerciseQuestions();