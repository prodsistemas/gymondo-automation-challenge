const expect = require('chai').expect;
const loginActions = require('../actions/login.actions');

class LoginQuestions {
    async loginFormShouldKeepDisplayed() {
        const isLoginFormDisplayed = loginActions.isLoginFormDisplayed();
        expect(isLoginFormDisplayed).equal(true);
    }
}
module.exports = new LoginQuestions();