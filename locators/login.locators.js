class LoginLocators {

    get userNameTxt() {return $('//android.widget.EditText[@resource-id="com.example.android.gymondoautomationtest:id/editText"]')};
    get passwordTxt() {return $('//android.widget.EditText[@resource-id="com.example.android.gymondoautomationtest:id/editText2"]')};
    get loginBtn() {return $('//android.widget.Button[@resource-id="com.example.android.gymondoautomationtest:id/button"]')};
}
module.exports = new LoginLocators();