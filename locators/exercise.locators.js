class Exercise {
    get exerciseImg() {return $('//android.widget.ImageView[@resource-id="com.example.android.gymondoautomationtest:id/appCompatImageView"]')};
    get exerciseTitle() {return $('//android.widget.TextView[@resource-id="com.example.android.gymondoautomationtest:id/item_text"]')}
}
module.exports = new Exercise();