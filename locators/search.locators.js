class Search {
    get searchTxt() {return $('//android.widget.EditText[@resource-id="com.example.android.gymondoautomationtest:id/editTxtSearch"]')};
    get searchBtn() {return $('//android.widget.ImageButton[@resource-id="com.example.android.gymondoautomationtest:id/btnSearch"]')}
    get clearBtn() {return $('//android.widget.ImageButton[@resource-id="com.example.android.gymondoautomationtest:id/btnClear"]')}
    get listItems() {return $$('//android.widget.TextView[@resource-id="com.example.android.gymondoautomationtest:id/item_text"]')};
}
module.exports = new Search();