# Gymondo Automation Challenge

## Pre-requirements
* Install and Run Appium.
* Install and Run Android Studio.
    * Create a new Virtual Device from AVD Manager.
    * Build and run the mobile application challenge that is in the following repository: https://github.com/Gymondo-git/mobile-android-automation-challenge
    * For running the mobile application, use the virtual device previously created.

## Setup the environment:
1. Fork the repository to your local machine.
2. From the root, install the Gymondo Automation Challenge dependencies by typing **npm install**.
3. Inside the ***wdio.config.js*** file, change the ***platformName*** and ***deviceName*** values in the capabilities section for the corresponding values  to the virtual device you created in Android Studio. In my case, I just named as 'Pixel'.

## Running and Reporting the tests results:
1. Open a bash terminal and run the tests by typing **npm run test**.
2. After the test execution is finished, generate the report by typing **npm run allure-report**.