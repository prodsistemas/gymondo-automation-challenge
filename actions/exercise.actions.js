const exerciseLocators = require('../locators/exercise.locators');

class ExerciseActions {
    async getExerciseTitle() {
        return await exerciseLocators.exerciseTitle.getText();
    }

    async isExerciseImageDisplayed() {
        return await exerciseLocators.exerciseImg.isDisplayed();
    }
}
module.exports = new ExerciseActions();