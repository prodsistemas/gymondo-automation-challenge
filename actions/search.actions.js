const searchLocators = require('../locators/search.locators');

class SearchActions {
    async searchForAnExercise(exercise) {
        await searchLocators.searchTxt.addValue(exercise);
        await searchLocators.searchBtn.click();
    }

    async resetSearch() {
        await searchLocators.clearBtn.click();
    }

    async getFirstResultTitleItem() {
        return await searchLocators.listItems[0].getText();
    }

    async selectFirstResultItem() {
        await searchLocators.listItems[0].click();
    }

    async getResultItemsSize() {
        return await searchLocators.listItems.length;
    }
}
module.exports = new SearchActions();