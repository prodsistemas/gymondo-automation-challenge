const loginLocators = require('../locators/login.locators');

class LoginActions {
    async loginToTheApplication(username, password) {
        await loginLocators.userNameTxt.clearValue();
        await loginLocators.userNameTxt.addValue(username);
        await loginLocators.passwordTxt.clearValue();
        await loginLocators.passwordTxt.addValue(password);
        await loginLocators.loginBtn.click();
    }

    async isLoginFormDisplayed() {
        return await loginLocators.loginBtn.isDisplayed();
    }
}
module.exports = new LoginActions();