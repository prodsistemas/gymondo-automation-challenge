Business Need: View Login accesibility

    All unregistered users can not access to the application

    Rule: Unregistered users can not access to the application
        Scenario Outline: Verify that Juan who is an unregistered user can not access to the application.
            When Juan try to access to the application using different invalid "<username>" and "<password>" credentials
            Then should deny the access to the application
            Examples:
                | username         | password         |
                | Invalid_Username | Invalid_Password |
                | Invalid_Username |                  |
                |                  | Invalid_Password |