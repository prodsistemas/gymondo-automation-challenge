const { Given, When, Then } = require('@cucumber/cucumber');
const searchActions = require('../../actions/search.actions');
const loginActions = require('../../actions/login.actions');
const exerciseQuestions = require('../../questions/exercise.questions');
const searchQuestions = require('../../questions/search.questions');
let expectedExercise;

Given('Pedro wants to search an exercise loged in as', async (dataTable) => {
    await loginActions.loginToTheApplication(dataTable.hashes()[0].username, dataTable.hashes()[0].password);
});

When('he search the existing exercise {string}', async (exercise) => {
    await searchActions.searchForAnExercise(exercise);
    await searchQuestions.searchResultItemsShouldBe(1);
    await searchActions.resetSearch();
    await searchQuestions.searchResultItemsShouldBeMoreThan(5);
    await searchActions.searchForAnExercise(exercise);
});

When('select one of the results', async () => {
    expectedExercise = await searchActions.getFirstResultTitleItem();
    await searchActions.selectFirstResultItem();
});

Then('the application should display the selected exercise content', async () => {
    await exerciseQuestions.shouldDisplaySelectedExerciseContent(expectedExercise);
});