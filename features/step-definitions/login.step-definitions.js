const { When, Then } = require('@cucumber/cucumber');
const loginActions = require('../../actions/login.actions');
const loginQuestions = require('../../questions/login.questions');

When('Juan try to access to the application using different invalid {string} and {string} credentials', async (username, password) => {
    await loginActions.loginToTheApplication(username, password);
});

Then('should deny the access to the application', async () => {
    await loginQuestions.loginFormShouldKeepDisplayed();
});