Business Need: View Search results

    All registered users can search for exercises that they like to take

    Rule: Registered users can search exercises that they would like to take
        Example: Verify that Pedro can select an exercise that he previously searched.
            Given Pedro wants to search an exercise loged in as
                | username              | password   |
                | automation@gymondo.de | automation |
            When he search the existing exercise "back squat"
            And select one of the results
            Then the application should display the selected exercise content